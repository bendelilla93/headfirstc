﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop
{
    class Program
    {
        static void Main(string[] args)
        {
            int p = 2;

            for (int q = 2; q < 32; q = q * 2)
            {
                while (p < q)
                {
                    p = p * 2;
                    Console.WriteLine("P = {0}", p);
                }

                q = p - q;
                Console.WriteLine("Q = {0}", q);
            }
            Console.ReadLine();
        }
    }
}
