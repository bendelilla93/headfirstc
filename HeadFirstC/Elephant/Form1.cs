﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elephant
{
    public partial class Form1 : Form
    {
        Elephant lucinda;
        Elephant llyod;

        public Form1()
        {
            InitializeComponent();
            lucinda = new Elephant() { Name = "Lucinda", EarSize = 33 };
            llyod = new Elephant() { Name = "Llyod", EarSize = 40 };
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            llyod.WhoAmI();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lucinda.WhoAmI();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Elephant holder;
            holder = llyod;
            llyod = lucinda;
            lucinda = holder;
            MessageBox.Show("Objects swapped");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            llyod.TellMe("Hi ", lucinda);
            llyod = lucinda;
            llyod.EarSize = 4321;
            llyod.WhoAmI();
        }
    }
}
