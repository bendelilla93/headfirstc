﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guy
{
    public partial class Form1 : Form
    { 
        private Guy joe;
        private Guy bob;
        private int bank = 100;

        public Form1()
        {
            InitializeComponent();
            joe = new Guy();
            joe.Name = "Joe";
            joe.Cash = 50;

            bob = new Guy();
            bob.Name = "Bob";
            bob.Cash = 100;

            UpdateForm();
        }

        public void UpdateForm()
        {
            label4.Text = joe.Name + "has $" + joe.Cash;
            label5.Text = bob.Name + "has $" + bob.Cash;
            label6.Text = "The bank has $" + bank;

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (bank <= 10)
            {
                bank -= joe.ReceiveCash(10);
                UpdateForm();
            }
            else
            {
                MessageBox.Show("The bank has no money.");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            bank += bob.ReceiveCash(5);
            UpdateForm();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            bob.ReceiveCash(joe.GiveCash(10));
            UpdateForm();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            joe.ReceiveCash(bob.GiveCash(5));
            UpdateForm();
        }
    }
}
