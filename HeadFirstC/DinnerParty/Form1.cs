﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DinnerParty;

namespace DinnerParty
{
    public partial class Form1 : Form
    {
         DinnerParty dinnerparty;
        public Form1()
        {
            InitializeComponent();
            dinnerParty = new DinnerParty((int)numericUpDown1.Value, healthyBox.Checked, fancyBox.Checked);
            dinnerparty.SetHealthyOption(false);
            dinnerparty.CalculateCost(true);
            DisplayDinnerPartyCost();
        }
        private void DisplayDinnerPartyCost()
        {
            decimal Cost = dinnerparty.CalculateCost(healthyBox.Checked);
            labelCost.Text = Cost.ToString("c");
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dinnerparty.NumberOfPeople = (int)numericUpDown1.Value;
            DisplayDinnerPartyCost();
        }

        private void labelCost_Click(object sender, EventArgs e)
        {

        }

        private void fancyBox_CheckedChanged(object sender, EventArgs e)
        {
            dinnerparty.CalculateCostOfDecorations(fancyBox.Checked);
            DisplayDinnerPartyCost();
        }

        private void healthyBox_CheckedChanged(object sender, EventArgs e)
        {
            dinnerparty.SetHealthyOption(healthyBox.Checked);
            DisplayDinnerPartyCost();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
